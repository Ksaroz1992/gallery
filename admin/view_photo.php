<?php require_once("includes/header.php"); ?>

<?php if (!$session->is_signed_in()){redirect("login.php");} ?>

<?php

if (empty($_GET['id']))
{
    redirect('photos.php');
} else {
    $photo = Photo::find_by_id($_GET['id']);

}

?>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->

    <?php include("includes/top_nav.php"); ?>

    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

    <?php include("includes/sidebar.php"); ?>

    <!-- /.navbar-collapse -->
</nav>

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    PHOTOS
                    <small>Subheading</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="photos.php">Photos list</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-file"></i> View Photo
                    </li>
                </ol>

                <form action="" method="post">
                <div class="col-md-8">

                    <div class="form-group">
                        <a href="#" class="img-thumbnail">
                            <img src="<?php echo $photo->picture_path(); ?>" alt="" width="400" height="400">
                        </a>
                    </div>
                </div>

                <div class="col-md-4" >
                    <div  class="photo-info-box">
                        <div class="info-box-header">
                            <h4>Photo Details<span id="toggle" class="glyphicon glyphicon-menu-up pull-right"></span></h4>
                        </div>
                        <div class="inside">
                            <div class="box-inner">
                                <p class="text">
                                    <span class="glyphicon glyphicon-calendar"></span> <?php echo "Uploaded on: " . date("Y/m/d") . " @ " . date("g:i a"); ?>
                                </p>
                                <p class="text ">
                                    Photo Id: <span class="data photo_id_box"><?php echo $photo->id; ?></span>
                                </p>
                                <p class="text">
                                    Filename: <span class="data"><?php echo $photo->filename; ?></span>
                                </p>
                                <p class="text">
                                    File Type: <span class="data"><?php echo $photo->type; ?></span>
                                </p>
                                <p class="text">
                                    File Size: <span class="data"><?php echo $photo->size; ?></span>
                                </p>
                            </div>
                            <div class="info-box-footer clearfix">
                                <div class="info-box-delete pull-left">
                                    <a  href="delete_photo.php?id=<?php echo $photo->id; ?>" class="btn btn-danger btn-lg ">Delete</a>
                                </div>
                                <div class="info-box-update pull-right ">
                                    <a href="photos.php" class="btn btn-primary btn-lg ">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>

            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->


</div>
<!-- /#page-wrapper -->

<?php include("includes/footer.php"); ?>
