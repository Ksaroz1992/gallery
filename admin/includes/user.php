<?php


class User extends Db_object
{

    public static $the_table = "users";

    protected static $the_table_field = ['username', 'password', 'first_name', 'last_name', 'user_image'];

    public $id;
    public $username;
    public $password;
    public $first_name;
    public $last_name;
    public $user_image;
    public $user_img_directory = "images";
    public $image_placeholder = "http://placehold.it/400x400&text=image";
    public $tmp_path;


    public function save_user_and_image()
    {
        if ($this->id) {

            $this->update();

        } else {

            if (!empty($this->errors)) {
                return false;
            }

            if (empty($this->user_image) || empty($this->tmp_path)) {
                $this->errors[] = "The file was not available";

                return false;
            }
        }

        $target_path = SITE_ROOT . DS . 'admin' . DS . $this->user_img_directory . DS . $this->user_image;

        if (file_exists($target_path)) {
            $this->errors[] = "The file {$this->user_image} already exists.";

            return false;
        }

        if (move_uploaded_file($this->tmp_path, $target_path)) {

            if ($this->create()) {

                unset($this->tmp_path);

                return true;
            }

        } else {
            $this->errors[] = "The file directory probably does not have a permissions";

            return false;
        }
    }

    public function image_path_and_placeholder()
    {
        return empty($this->user_image) ? $this->image_placeholder : $this->user_img_directory . DS . $this->user_image;
    }

    public static function verify_user($username, $password)
    {
        global $database;

        $username = $database->escape_string($username);
        $password = $database->escape_string($password);

        $sql = " SELECT * FROM " . self::$the_table . " WHERE ";
        $sql .= " username = '{$username}' ";
        $sql .= " AND password = '{$password}' ";

        $result_set_array = self::find_this_query($sql);

        //***********using ternary function*****************//

        return !empty($result_set_array) ? array_shift($result_set_array) : false;

    }

    public function user_picture_path()
    {
        return $this->user_img_directory.DS.$this->user_image;
    }

    public function delete_user()
    {
        if ($this->delete())
        {
            $target_path = SITE_ROOT.DS . 'admin' . DS . $this->user_picture_path();

            return unlink($target_path) ? true : false;
        } else {
            return false;
        }
    }


} // End of class User
