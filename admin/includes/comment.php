<?php


class Comment extends Db_object
{

    public static $the_table = "comments";

    protected static $the_table_field = ['photo_id', 'author', 'body'];

    public $id;
    public $photo_id;
    public $author;
    public $body;


    public static function create_comment($photo, $author, $body)
    {
        if(!empty($photo) && !empty($author) && !empty($body))
        {
            $comment = new Comment();

            $comment->photo_id = (int)$photo;
            $comment->author = $author;
            $comment->body = $body;

            return $comment;
        } else {
            return false;
        }
    }

    public static function find_the_comments($photo_id)
    {
        global $database;

        $sql = "SELECT * FROM " . self::$the_table;
        $sql.= " WHERE photo_id =" . $database->escape_string($photo_id);
        $sql.= " ORDER BY photo_id DESC";

        return self::find_this_query($sql);
    }


} // End of class User
