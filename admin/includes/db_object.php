<?php


class Db_object
{
    public $errors = array();
    public $upload_errors = [

        UPLOAD_ERR_OK => "There is no error",
        UPLOAD_ERR_INI_SIZE => "The uploaded file exceeds the upload_max_filesize directive",
        UPLOAD_ERR_FORM_SIZE => "The uploaded file exceeds the MAX_FILE_SIZE directive",
        UPLOAD_ERR_PARTIAL => "The uploaded file was only partially uploaded.",
        UPLOAD_ERR_NO_FILE => "No file was uploaded",
        UPLOAD_ERR_NO_TMP_DIR => "Missing a temporary file",
        UPLOAD_ERR_CANT_WRITE => "Failed to write file to disk",
        UPLOAD_ERR_EXTENSION => "A php extension stopped the file upload"
    ];



    // This is passing $_FILE['user_image'] as an argument

    public function set_file($file)
    {
        if (empty($file) || !$file || !is_array($file)) {
            $this->errors[] = "There was no file uploaded here";
            return false;

        } elseif ($file['error'] != 0) {
            $this->errors[] = $this->upload_errors[$file['error']];
            return false;
        } else {

            $this->user_image = basename($file['name']);
            $this->tmp_path = $file['tmp_name'];
            $this->type = $file['type'];
            $this->size = $file['size'];
        }
    }

    public static function find_all()
    {
//        global $database;
//        $result_set = $database->query("SELECT * FROM users");
//        return $result_set;

        return static::find_this_query("SELECT * FROM " . static::$the_table . " ");
    }

    public static function find_by_id($id)
    {
        global $database;
//        $result_set = $database->query("SELECT * FROM users WHERE id=$id LIMIT 1");
        $result_set_array = static::find_this_query("SELECT * FROM " . static::$the_table . " WHERE id=$id LIMIT 1");

        //***********using ternary function*****************//

        return !empty($result_set_array) ? array_shift($result_set_array) : false;

        //****using if-else function ****//

//        if(!empty($result_set_array))
//        {
//            $first_item = array_shift($result_set_array);
//            return $first_item;
//        }else{
//            return false;
//        }


    }

    public static function find_this_query($sql)
    {
        global $database;
        $result_set = $database->query($sql);
        $the_object_array = array();

        while($row = mysqli_fetch_array($result_set))
        {
            $the_object_array[] = static::instantiation($row);
        }
        return $the_object_array;
    }

    private function has_the_attribute($the_attribute)
    {
        $object_properties = get_object_vars($this);

        return array_key_exists($the_attribute, $object_properties);
    }

    public static function instantiation($the_record)
    {
        $calling_class = get_called_class();

        $the_object = new $calling_class;

//        $the_object->id = $the_record['id'];
//        $the_object->username = $the_record['username'];
//        $the_object->password = $the_record['password'];
//        $the_object->first_name = $the_record['first_name'];
//        $the_object->last_name = $the_record['last_name'];

        foreach ($the_record as $the_attribute => $value)
        {
            if($the_object->has_the_attribute($the_attribute))
            {
                $the_object->$the_attribute = $value;
            }
        }

        return $the_object;
    }

    protected function properties()
    {
        $properties = array();

        foreach (static::$the_table_field as $the_field)
        {
            if (property_exists($this, $the_field))
            {
                $properties[$the_field] = $this->$the_field;
            }
        }
        return $properties;
    }

    protected function clean_properties()
    {
        global $database;
        $clean_properties = array();

        foreach ($this->properties() as $key => $value)
        {
            $clean_properties[$key] = $database->escape_string($value);
        }

        return $clean_properties;
    }

    public function save()
    {
        return ($this->id) ? $this->update() : $this->create();
    }

    public function create()
    {
        global $database;
        $properties = $this->clean_properties();

        $sql = "INSERT INTO " . static::$the_table . "(" . implode(",", array_keys($properties)) . ")";
        $sql .= "VALUES ('". implode("','", array_values($properties)) . "')";


        if ($database->query($sql))
        {
            $this->id = $database->the_insert_id();
            return true;
        } else {
            return false;
        }
    } // End of method create

    public function update()
    {
        global $database;

        $properties = $this->clean_properties();

        $properties_pairs = array();
        foreach ($properties as $key => $value)
        {
            $properties_pairs[] = "{$key}='{$value}'";
        }
        $sql = "UPDATE " . static::$the_table . " SET ";
        $sql .= implode(", ", $properties_pairs);
        $sql .= " WHERE id= " . $database->escape_string($this->id);

        $database->query($sql);

        return (mysqli_affected_rows($database->connection) == 1) ? true : false;
    }

    public function delete()
    {
        global $database;
        $sql = "DELETE FROM " . static::$the_table . " ";
        $sql .= "WHERE id=" . $database->escape_string($this->id);
        $sql .= " LIMIT 1";

        $database->query($sql);

        return (mysqli_affected_rows($database->connection) == 1) ? true : false;
    }

    public static function count_all()
    {
        global $database;

        $sql = "SELECT COUNT(*) FROM " . static::$the_table;
        $result_set = $database->query($sql);
        $row = mysqli_fetch_array($result_set);

        return array_shift($row);
    }



}
