This is a CMS project which is about a picture gallery. It is based on PHP OOP programming language. 
This project has a feature of uploading images and CRUD on it through admin panel "localhost/gallery/admin",
whereas in home page images are listed and user can view individual image details and comment on it.

--need to do on this project--
--frontend--
1. like button, total views etc yet to add.
2. search, details of time etc yet to work.
3. others new functionality yet to add/work.

--backend--

1. yet to work/improve on design part.


This project is done by Saroj Kumar Shrestha on the year of 2019.
during his academic year.

The sql file is provided inside the directories>sql in this repository.

Thank you.